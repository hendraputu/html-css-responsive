# Section 1: Welcome and First Steps
# Section 2: HTML Fundamentals
## 12. Text Elements
```
<b>bold but this is old markup</b>
<strong>strong is simantic html same like b but more meaningful</strong>
<i>i = italic but old element</i>
<em>em, emphasis same like italic</em>
```
## 13. More Text Elements: Lists
ol = order list
1. First
2. Second
3. Third

ul = unorder list
- First item
- Second item
- Third item
- Fourth item
```
// ol
<ol>
  <li>First item</li>
  <li>Second item</li>
  <li>Third item</li>
  <li>Fourth item</li>
</ol>
//
<ul>
  <li>First item</li>
  <li>Second item</li>
  <li>Third item</li>
  <li>Fourth item</li>
</ul>
```
## 17. A Note on Semantic HTML
So in HTML, when we talk about semantics,
what we mean is that certain elements
have actually a meaning or a purpose attached to them.
So when we think about a certain HTML element,
we should actually not think
about what that element looks like
as it's rendered on the page
but instead, we should think
about what that element actually means
and what it stands for.
So that is basically the definition of semantic HTML.
### Why semantic HTML is important?
we know, we can replace `<p>` by `<div>` or `<nav`> by `<div>` as well, it display with stay the similar but not the meaning. When we write paragraph, it's better to use `<p>` element. And so on. And also semantic HTML is important for SEO.

# Section 3: CSS Fundamentals
## 23. Inline, Internal and External CSS
### Inline
```
<h1 style="color:blue">Inline</h1>
```
### Internal
```
<html>
  <head>
    <title>Document</title>
    <style>
      h1 { color: blue; }
    <style>
  </head>
  ...
```
### External
Write style css on separe file, we call it with separation of concerns. This is the best way of writing css and we should avoid write inline css.
```
<html>
  <head>
    <title>Document</title>
    <link href="style.css" rel="stylesheet" />
    ...
```
## 24. Styling Text
The best practice of styling css is by using order and consistent with it. Example:
```
h1 {
  font-size: 26px; // first order
  font-family: sans-serif; // second order
  text-transform: uppercase;
  font-style: italic;
}

h2 {
  font-size: 40px; // first order
  font-family: sans-serif; // second order
}
```
## 25. Combining Selectors
Instead of repeat `font-family` for every element, then it's better to use combining selectors
```
h1, h2, p {
  font-family: sans-serif;
}
```
`p` element has own `font-size` and it will apply for all `p` element, but we need `p` element in footer little bit smaller. So we need to combine `footer` and `p` element like this:
```
// this is called the descendant selector
// then p element is child of footer element
footer p {
  font-size: 9px;
}
```
## 26. Class and ID Selectors
ID only use for one element, but Class we can reuse for any element.
For best practice we should use class ether for one element or any element to prevent bugs in the future.
## 28. Pseudo-classes
There is the case when we need to styling first list, second list or last list. So we have to use pseudo-class. Here example:
```
li:first-child {
  font-weight: bold; // it will bold first list
}
li:last-child {
  font-style: italic;
}
li:nth-child(2) { // make second list to be text red
  color: red;
}
li:nth-child(even) { // make even number to be blue. (2, 4, 6...)
  color: blue;
}
```
but there is the case won't work like below example:
```
// html
<article>
  <h1>This is header</h1>
  <p>This is a first paragraph</p>
  <p>This is a second paragraph</p>
</article>

// css
article p:first-child {
  color: red;
}
```
Why the case above won't work? because first element is `h1` not `p`
## 29. Styling Hyperlinks
The basic styling of hyperlink are pseudo-classes: a:link, a:visited, a:hover, and a:active
```
a:link { // use this link targeted instead of only a
  color: red;
  text-decoration: none;
}
```
## 31. CSS Theory 1: Conflict Between Selectors
Priority from Highest to lowest:
1. Declarations marked !important
2. inline style (style attribute in HTML)
3. ID (#) selector
4. Class (.) or pseudo-class (:) selector
5. Element selector (p, div, li, etc)
6. Universal selector (*)
if there is a case like below example then css will apply base on priority
```
.author {
  font-style: italic;
  font-size: 18px;
}
#author-text {
  font-size: 20px;
}
p {
  font-family: sans-serif;
  font-size: 22px;
}
// html
<p id="author-text" class="author">this is paragraph</p>
```
## 32. CSS Theory 2: Inheritance and the Universal Selector
```
// body is a parent element
body {
  color: red;
}
// p is a one of child element
p {
  color: blue;
  font-size: 12px;
}
```
`p` will overwrite `color` of `body`, but if p doesn't define `color` then, `p` will use `color` from parent (body).
This example below, (*) will apply for all elements. but we still can overwrite.
```
* {
  font-family: sans-serif;
}
body {
  color: black;
}
```
## 39. CSS Theory 4: Types of Boxes
See explanation on slide (theory-lectures-v2) page 51
As a default every elements will apply `display:block;` we can change base our need.
`block` will display vertical, `inline` horizontal, `inline-block` horizontal but apply margin like `block`.
We can only add margin horizontal and padding horizontal.
## 40. CSS Theory 5: Absolute Positioning
Absolute position mean we can place box to everywhere, but first we need to set posittion relative to parent element. Absolute position will work if we define `top`, `left`, `right`, `bottom` value of properties.
## 41. Pseudo-elements
We can define pseudo-class by using (:), but for pseudo-element we can define by using (::). Example we want to add style after the content.
```
h1::after {
  content: "TOP"
}

// html
<h1>This is pseudo-element</h1> // output: This is pseudo-elementTOP
```

# Section 4: Layouts: Floats, Flexbox, and CSS Grid Fundamentals
## 46. The 3 Ways of Building Layouts
There are two main of Layout, *Page Layout* & *Component Layout*
The 3 ways of building layouts with css:
- Float Layouts (old way)
- Flexbox
- CSS Grid
**look at theory-lectures-v2-BEST (page 60) for more explanation**
## 54. Spacing and Aligning Flex Items
`align-items` property is use to align vertically. 
`justify-content` property is use to align horizontally.
Both of 2 properties are used in flex container. Example:
```
.container {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
```
## 55. The flex Property
about flex items:
```
flex-basis: 200px; // will set all boxes to 200px instead set width to all boxes
flex-shrink: 1; // actually the value are 0 or 1; the behaviour same like box-sizing
flex-grow: 1; // default value is 0, we can set to 0, 1, 2, the behaviour will increase the width to fit the rest of content
```
but the shorthand we can use for those 3 properties above are just use this:
`flex: 1;` // then all items will be the same size.
## 57. Building a Simple Flexbox Layout
When we work with flex box to styling component and we want to add width for a item, the best practice is by adding flex-basis instead of width. Example:
```
aside {
  flex-grow: 0;
  flex-shrink: 1; // like box-sizing: border-box
  flex-basis: 300px; // like width
  // shorthand 3 properties are just flex: 0 0 300px;
}
```
If we need to add space between boxes, so we just add gap in flex container instead of add margin. Example:
```
.container {
  display: flex;
  gap: 10px;
}
```
To make hight of box flexible with the content, we no neet to set hight, but we can leverage `align-item: flex-start;` in flex container. Example:
```
.container {
  display: flex;
  align-items: flex-start;
}
```
## 59. Introduction to CSS Grid
to define layout with CSS Grid, same like flexbox, we can use `display` property. As same like flexbox, in CSS Grid we use container and items. Container is a parent element, and items are children.
```
.container {
  display: grid;
  grid-template-columns: 200px 200px 100px 100px;
  grid-template-rows: 300px 200px;
  /* gap: 30px; we can use this if column-gap and row-gap are the same */
  /* always use gap or column-gap not margin */
  column-gap: 30px;
  row-gap: 60px;
}
```
we can define width of every column by using `grid-template-columns`. Or we can define specific rows by using grid-template-rows.
## 60. A CSS Grid Overview
What is CSS Grid? CSS Grid is a set of CSS properties for building 2-dimensional layouts and usually we can use to build page layout. So if you need a 1D layout, then use flexbox. Need a 2D layout? use CSS Grid.
In css grid, there are cells and we don't have to fill all cells. For more details see pdf document.
## 61. Sizing Grid Columns and Rows
So how to make size flexible when we resize the browser windows?
In flexbox we can use `flex: 1;`. In css grid we can use `fr`.
```
grid-template-columns: 100px 1fr;
grid-template-columns: repeat(3, 1fr); /* 1fr 1fr 1fr */
```
we can apply `fr` for `grid-template-rows` as well.
## 62. Placing and Spanning Grid Items
To make easy placing and spanning grid items, we can leverage dev tools by inspect element and click grid on inspect element. So it will show the visualization of grid line.
We can use these 2 properties to span or place grid items: `grid-column` and `grid-row`.
```
.el-1 {
  grid-column: 2 / 3; // line 2 to 3;
  grid-row: 1 / 2;
}

.el-2 {
  grid-column: 1 / 3; // it will be spanning the column;
  grid-row: 1;
}
```
## 63. Aligning Grid Items and Tracks
Aligning grid items and track will work if we have empty space in container, in other word container all items is small than container. Track is group of all items.
We can place to center of container by doing this:
```
.container {
  display: grid;
  justify-content: center; /* aligning horizontally */
  align-content: center; /* aligning vertically */
}
```
We can also aligning only one item or more, by using `align-self` & `justify-self`. But this case will work when we have empty space in grid cell.
```
.el-1 {
  align-self: center;
  justify-self: center;
}
```
# Section 5: Web Design Rules and Framework
# Section 6: Components and Layout Patterns
# Section 7: Omnifood Project - Setup and Desktop Version
# Section 8: Omnifood Project - Responsive Web Design
# Section 9: Omnifood Project - Effects, Optimizations and Deployment
# Section 10: The End!
# Section 11: Legacy - Old Course Version 1